﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ModelInfoCenter;
using ModelSimpleSMS.ModelWork;

namespace ModelSimpleSMS.Interface
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private ModelSettings GetSettings()
        {
            try
            {
                var settings = new ModelSettings();
                settings.UniformSettings.Left = Convert.ToDouble(textBox1.Text);
                settings.UniformSettings.Right = Convert.ToDouble(textBox2.Text);
                settings.NormalSettings.Mean = Convert.ToDouble(textBox3.Text);
                settings.NormalSettings.Deviation = Convert.ToDouble(textBox4.Text);
                settings.ProbabilityOfReturn = Convert.ToDouble(textBox5.Text);
                settings.DeltaT = Convert.ToDouble(textBox6.Text);
                settings.SystemWorkingTimeLimit = Convert.ToDouble(textBox7.Text);

                return settings;
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка считывания данных");
            }
            return null;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            var settings = GetSettings();
            if (settings != null)
            {
                ModelManager.Instance.Initialize(settings);
                ModelManager.Instance.ModelWholeSystem();

                var queueLength = ModelManager.Instance.GetStorageQueueLength();
                textBox8.Text = queueLength.ToString();
            }

        }
    }
}
