﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelInfoCenter.ModelWork;
using ModelSimpleSMS.Entities;
using ModelSimpleSMS.ModelWork;

namespace ModelInfoCenter
{
    public class ModelManager
    {
        #region Members
        
        private static volatile ModelManager _instance;
        private static object _syncRoot = new Object();

        // Scheme
        private InformationSource infoSource;
        private Storage storage;
        private ServingMachine servingMachine;

        // Model Paameters
        private List<ModelingEvent> modelingEvents;
        private StatisticsModule statistics;
        private double currentTime;
        private double dt;

        #endregion

        #region Constructors
        private ModelManager() { }

        public static ModelManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_syncRoot)
                    {
                        if (_instance == null)
                            _instance = new ModelManager();
                    }
                    //ModelManager.Instance.Initialize();
                }

                return _instance;
            }
        }

        public void Initialize(ModelSettings settings)
        {
            CreateScheme(settings);
            CreateStatistics(settings);
            ConnectEventsInScheme();
            InitializeParameters(settings);
        }
        
        private void CreateScheme(ModelSettings settings)
        {
            servingMachine = new ServingMachine(settings.NormalSettings);
            storage = new Storage(servingMachine);
            infoSource = new InformationSource(storage,settings.ProbabilityOfReturn,settings.UniformSettings);
        }

        private void CreateStatistics(ModelSettings settings)
        {
            statistics = new StatisticsModule(settings.SystemWorkingTimeLimit);
        }

        private void ConnectEventsInScheme()
        {
            infoSource.InformationSourceEvent += AddModelingEvent;
            servingMachine.EndOfServingRequest += AddModelingEvent;
            servingMachine.RequestServed += infoSource.OnRequestProcessed;
        }

        private void InitializeParameters(ModelSettings settings)
        {
            modelingEvents = new List<ModelingEvent>();
            currentTime = 0;
            dt = settings.DeltaT;
        }

        #endregion

        #region Methods

        public void ModelWholeSystem()
        {
            infoSource.CreateRequest();
            while (statistics.DoContinueModeling(currentTime))
            {
                ExecuteCurrentEvents();
                currentTime += dt;
            }
        }

        private void ExecuteCurrentEvents()
        {
            List<ModelingEvent> currentEvents = GetCurrentEvents();
            foreach (var modelingEvent in currentEvents)
            {
                modelingEvent.CallFunction(modelingEvent.Request);
                modelingEvents.Remove(modelingEvent);
            }
        }

        private List<ModelingEvent> GetCurrentEvents()
        {
            List<ModelingEvent> currentEvents = modelingEvents.Where(ev => ev.NextCallTime >= currentTime && ev.NextCallTime <= currentTime+dt).ToList();
            return currentEvents;
        }

        public void AddModelingEvent(object sender, ModelingEventArgs modelingEventArgs)
        {
            var modelEvent = modelingEventArgs.ModelEvent;
            modelEvent.NextCallTime += currentTime;
            
            var nearestEventIndex = modelingEvents.FindIndex(ev => modelEvent.NextCallTime < ev.NextCallTime);
            if (nearestEventIndex != -1)
            {
                modelingEvents.Insert(nearestEventIndex, modelEvent);
            }
            else
            {
                modelingEvents.Add(modelEvent);
            }
        }

        public int GetStorageQueueLength()
        {
            return storage.GetQueueLength();
        }

        #endregion
    }
}
