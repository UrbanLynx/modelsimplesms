﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelInfoCenter
{
    public class ModelingEvent
    {
        public ModelingEvent(Action<Request> callFunction, double nextCallTime, Request request)
        {
            CallFunction = callFunction;
            NextCallTime = nextCallTime;
            Request = request;
        }
        public double NextCallTime { get; set; }
        public Action<Request> CallFunction { get; set; }
        public Request Request { get; set; }
    }

    public class ModelingEventArgs : EventArgs
    {
        public ModelingEventArgs(Action<Request> callFunction, double nextCallTime, Request request)
        {
            ModelEvent = new ModelingEvent(callFunction, nextCallTime,request);
        }
        public ModelingEvent ModelEvent { get; set; }
    }
}
