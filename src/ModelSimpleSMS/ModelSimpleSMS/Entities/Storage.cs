﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelInfoCenter;

namespace ModelSimpleSMS.Entities
{
    class Storage
    {
        private Queue<Request> requests;
        private int queueLength;
        private ServingMachine machine;

        public Storage(ServingMachine servingMachine)
        {
            machine = servingMachine;
            machine.RequestServed += MachineOnRequestServed;
            requests = new Queue<Request>();
            queueLength = 0;
        }

        private void MachineOnRequestServed(object sender, Request request)
        {
            SendNextRequestToServingMachine();
        }

        public void AddRequest(Request request)
        {
            if (IsMemoryEmpty() && machine.IsFree())
            {
                machine.BeginServeRequest(request);
            }
            else
            {
                Enqueue(request);
            }
        }

        public int GetQueueLength()
        {
            return queueLength;
        }

        private void Enqueue(Request request)
        {
            requests.Enqueue(request);
            queueLength++;
        }

        private Request Dequeue()
        {
            return requests.Dequeue();
        }

        private bool IsMemoryEmpty()
        {
            return requests.Count == 0;
        }

        private void SendNextRequestToServingMachine()
        {
            if (!IsMemoryEmpty() && machine.IsFree())
            {
                var request = Dequeue();
                machine.BeginServeRequest(request);
            }
        }
    }
}
