﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelInfoCenter;
using ModelInfoCenter.Generator;
using ModelInfoCenter.ModelWork;
using ModelSimpleSMS.ModelWork;

namespace ModelSimpleSMS.Entities
{
    class InformationSource
    {
        private Storage storage;
        private double probabilityOfReturn;
        private UniformDistributionSettings uniformDistributionSettings;

        public event EventHandler<ModelingEventArgs> InformationSourceEvent;

        public InformationSource(Storage stor, double returnProbability, UniformDistributionSettings settings)
        {
            storage = stor;
            probabilityOfReturn = returnProbability;
            uniformDistributionSettings = settings;
        }

        public void OnRequestProcessed(object sender, Request request)
        {
            var probability = RandomGenerator.GetUniform(0, 1);
            if (probability < probabilityOfReturn)
            {
                storage.AddRequest(request);
            }
        }

        public void CreateRequest()
        {
            GenerateNextRequestComeTime();
        }

        public void SendRequest(Request request)
        {
            storage.AddRequest(request);
            CreateRequest();
        }

        private void GenerateNextRequestComeTime()
        {
            var nextTime = RandomGenerator.GetUniform(uniformDistributionSettings.Left, uniformDistributionSettings.Right);
            InformationSourceEvent(this, new ModelingEventArgs(SendRequest, nextTime, new Request()));
        }



    }
}
