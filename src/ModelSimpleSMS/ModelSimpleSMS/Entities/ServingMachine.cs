﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelInfoCenter;
using ModelInfoCenter.Generator;
using ModelSimpleSMS.ModelWork;

namespace ModelSimpleSMS.Entities
{
    class ServingMachine
    {
        private bool isFree;
        private NormalDistributionSettings normalDistributionSettings;

        public event EventHandler<ModelingEventArgs> EndOfServingRequest;
        public event EventHandler<Request> RequestServed; 

        public ServingMachine(NormalDistributionSettings settings)
        {
            normalDistributionSettings = settings;
            isFree = true;
        }

        public bool IsFree()
        {
            return isFree;
        }

        public void BeginServeRequest(Request request)
        {
            GenerateServingRequestTime(request);
            isFree = false;
        }

        private void GenerateServingRequestTime(Request request)
        {
            var nextTime = RandomGenerator.GetNormal(normalDistributionSettings.Mean, normalDistributionSettings.Deviation);
            nextTime = Math.Abs(nextTime);
            EndOfServingRequest(this, new ModelingEventArgs(FinishServingOfRequest, nextTime, request));
        }

        public void FinishServingOfRequest(Request request)
        {
            isFree = true;
            RequestServed(this, request);
        }
    }
}
